/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* mainWindow.h : main window code */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H 1


#include <objc/objc.h>
#include <objc/objc-api.h>
#include <objc/Object.h>
#include <gtk/gtk.h>

#include <gnome.h>
#include <obgnome/obgnome.h>

#include <config.h>
#include "mbTreeWin.h"


#endif /* MAIN_WINDOW_H */
