/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* mbTreeWin.h : window containing the mailbox tree */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef MB_TREE_WIN_H
#define MB_TREE_WIN_H 1


#include <objc/objc.h>
#include <objc/objc-api.h>
#include <objc/Object.h>
#include <gtk/gtk.h>

#include <gnome.h>
#include <obgnome/obgnome.h>

#include <config.h>


@interface Mailbox_Tree_Window : Gtk_ScrolledWindow 
{
    Gtk_CTree *mailboxTree;
    Gtk_Frame *frame;
    Gtk_Adjustment *hAdjustment;
    Gtk_Adjustment *vAdjustment;


}
- init;

@end


#endif /* MB_TREE_WIN_H */
