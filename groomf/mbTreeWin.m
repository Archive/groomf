/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* mbTreeWin.h : window containing the mailbox tree */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <mbTreeWin.h>

static gchar *mbListTitles[3] = { "toto", "titi", NULL};

@implementation Mailbox_Tree_Window


- init
{
    hAdjustment = [[Gtk_Adjustment alloc] initWithAdjustmentInfo :0 :0 :0 :0 :0 :0];
    vAdjustment = [[Gtk_Adjustment alloc] initWithAdjustmentInfo :0 :0 :0 :0 :0 :0];

    [super initWithScrolledWindowInfo:hAdjustment :vAdjustment];
    mailboxTree = [[Gtk_CTree alloc] initWithTitles:2 :1 :mbListTitles];
    frame = [[[Gtk_Frame alloc]  initWithFrameInfo:""] show];
    [frame set_shadow_type: GTK_SHADOW_IN];
    [self add:mailboxTree];
    
    [mailboxTree show];
    return self;
}
@end

