/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* mainWindow.m : main window code */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PACKAGE
#define PACKAGE "groomf"
#endif

#include "mainWindow.h"

/////////////////////////////////////////////////////////
// Menus definition 

GnomeUIInfo filemenu[] = {
  GNOMEUIINFO_MENU_EXIT_ITEM("quit", NULL),
  GNOMEUIINFO_END
};

GnomeUIInfo helpmenu[] = {
  GNOMEUIINFO_HELP("obHello"),
  GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = {
  GNOMEUIINFO_MENU_FILE_TREE(&filemenu),
  GNOMEUIINFO_MENU_HELP_TREE(&helpmenu),
  GNOMEUIINFO_END
};

/////////////////////////////////////////////////////////



// Main window
@interface Main_Window : Gnome_AppWin
{
    id application;
    Gtk_HPaned *mainHPaned;
    Mailbox_Tree_Window *mailboxTreeWindow;
    
}
- initForApp: (id) app;
- quit: (id)anObj;
- delete_event:(id) myObj :(GdkEventAny *) event;
@end

@implementation Main_Window
- initForApp: (id) app
{
	application = app; 

	self = [super initWithAppWinInfo:"groomf" :_("Groomf : a GNOME mail client")];

	// add main menu bar
	[self create_menus:mainmenu];
		
	// create the main hpaned
	mainHPaned = [Gtk_HPaned new];
	[mainHPaned show];
	
	// add it to the main window
	[self set_contents:mainHPaned];

	// create the mailbox list window
	mailboxTreeWindow = [Mailbox_Tree_Window new];
	[mailboxTreeWindow show];
	[mainHPaned add1:mailboxTreeWindow];
	[mainHPaned set_handle_size:7];
	[mainHPaned set_gutter_size:8];

	[self connect:"delete_event"];
	return self;
}

- quit: (id)anObj
{
	[application  quit];
	return self;
}

- delete_event:(id) myObj :(GdkEventAny *) event
{
	return [self quit:myObj];
}

@end






////////////////////////////////////////////////

// main object : the application object

@interface Groomf_App : Gnome_App
{
     Main_Window *window;
}
-initGroomf:(char *)app_id :(int) argc :(char **) argv;
@end

@implementation Groomf_App
-initGroomf:(char *)app_id
	:(int) argc
	:(char **) argv
{
	self = [super initApp:app_id :VERSION :argc :argv];

	window = [[[Main_Window alloc] initForApp:self] show];

	return self;
}
@end



gint
main(int argc, char *argv[])
{

    Groomf_App *groomfApp;
    
    bindtextdomain(PACKAGE, GNOMELOCALEDIR);
    textdomain(PACKAGE);
    groomfApp = [Groomf_App alloc];
    [groomfApp initGroomf:"groomf" :argc :argv];
    [groomfApp run];
    [groomfApp free];
    
    return 0;

}
