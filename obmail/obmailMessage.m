/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailMessage.m : Abstract class for an email message */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <obmailMessage.h>
@implementation Obmail_Message

/**
 * init:
 * Message object constructor 
 */
- init
{
    [super init];
    flags = [Obmail_Flags new];
    recipientsTable = [Obuli_Str_Obj_Hash_Table new];
    return self;
}

/**
 * setFlag:
 * set/unset a state flag
 * 
 * @name: name of the flag
 * @value: value to give to the flag.
 */
- setFlag: (gchar *)name
    value: (gboolean)value
{
    [flags setFlag:name value:value];
    return self;
}


/**
 * getFlag:
 * get a state flag value
 * 
 * @name: name of the flag
 *
 * @Return value: value of the flag.
 */
- (gboolean)getFlag: (gchar *)name
{
    return [flags getFlag:name];
    
}



/**
 * setFrom:
 * set the "From" field of the message
 *
 * from : "From" address  to give to the message object.
 */ 
- setFrom: (Obmail_Address *)addr
{
    g_assert(addr);
    from = [addr clone];
    return self;
}

/**
 * getFrom:
 * get the "From" field of the message
 *
 * @Return Value: "From" address of the message.
 */ 
- (Obmail_Address *)getFrom
{
     return [from clone];
}




/**
 * setMessageNumber:
 * set the number of the message in the folder in which
 * if lives. This method is not intended to be called 
 * by external programs. 
 *
 * @msgnum: number of the message;
 */
- setMessageNumber: (gint)msgnum
{
    msgNumber = msgnum;
    return self;
}


/**
 * getMessageNumber:
 * get the number of the message in the folder in which
 * if lives. This method is not intended to be called 
 * by external programs. 
 *
 * @Return value: number of the message;
 */
- (gint)getMessageNumber
{
    return msgNumber;
}

#warning ##########
#warning the single address will have to be replaced 
#warning by a list of addresses.
#warning ##########

- setRecipient: (gchar *)recType
       address: (Obmail_Address *)address
{
      gchar *origType;
      Obmail_Address *origAddress;
      gboolean origExists;
      gchar *typeName;
      
      g_assert(recType);
      g_assert(address);
      
      typeName = g_strdup(recType);
      // we don't free typeName cause it will be used as the new key
      origExists = [recipientsTable lookupExtendedKey: (id)typeName
				    origKey:(id *)&origType 
				    origValue:(id *)&origAddress];
      
      if (origExists)
	  {
	      // origType is a string, not an object
	      g_free(origType);
	      // has to become a list
	      // when this is a list, it has to be changed to be the
	      // specific address array  destructor
	      [origAddress free];
	  }
      
      
      [recipientsTable insertKey:(id)typeName value:[address clone] ];
      
      return self;
}


/**
 * getRecipient: 
 * get the recipient address of a given type
 * 
 * If the given recipient type does not exist, the method
 * returns NULL.
 *
 * @recType: type of the recipient looked up
 *
 * @Return value: the address corresponding to the given
 * recipient type.
 *
 */
- (Obmail_Address *)getRecipient: (gchar *)recType
{
      gpointer origType;
      Obmail_Address *origAddress;
      gboolean origExists;
      Obmail_Address *recipient;
      
      g_assert(recType);
      
      origExists = [recipientsTable lookupExtendedKey: (id)recType
				    origKey:(id *)&origType 
				    origValue:(id *)&origAddress];
      

      if (origExists)
	  recipient = [origAddress clone];
      else
	  recipient = NULL;

      return recipient;
}


- setSubject: (Obuli_String *)subj
{
    subject = [subj clone];
    return self;
}

/**
 * setReceivedDate:
 *
 * set the date the message was received 
 * 
 * @date: the date the message was received
 */
- setReceivedDate: (Obuli_Date *)date
{
    g_assert(date);
    g_assert([date valid]);
    if (receivedDate) [receivedDate free];

    receivedDate = [date clone];
    return self;
}


/**
 * getReceivedDate:
 *
 * get the date the message was received 
 * 
 * @Return Value: the date the message was received
 */
- (Obuli_Date *)getReceivedDate
{
    
    if (!receivedDate) return NULL;

    g_assert([receivedDate valid]);
    return [receivedDate clone];
}

/**
 * setSentDate:
 *
 * set the date the message was sent 
 * 
 * @date: the date the message was sent
 */
- setSentDate: (Obuli_Date *)date
{
    g_assert(date);
    g_assert([date valid]);
    sentDate = [date clone];
    return self;
}


/**
 * getSentDate:
 *
 * get the date the message was sent 
 * 
 * @Return Value: the date the message was sent
 */
- (Obuli_Date *)getSentDate
{
    
    if (!sentDate) return NULL;

    g_assert([sentDate valid]);
    return [sentDate clone];
}




/** initAsClone:
 * Internal use only. When an object is copied, 
 * instance variables are copied  only as a structure.
 * This method is here to copy the references of 
 * the instance variables pointer.
 **/
- initAsClone
{
    if (subject) subject = [subject clone];
    if (sentDate) sentDate = [sentDate clone];
    if (receivedDate) receivedDate = [receivedDate clone];
    if (from) from = [from clone];
    if (flags) flags = [flags clone];
    if (recipientsTable) recipientsTable = [recipientsTable clone];
    return self;
}

- clone
{
    Obmail_Message *instanceClone;

    instanceClone = [super copy];
    [instanceClone initAsClone];
    return instanceClone;
}


// used in g_hash_table_foreach to free a recipient entry
void  freeRecipient(gpointer key, gpointer value, gpointer user_data)
{
    g_free(key);
    [(Obmail_Address *)value free];
}

- free
{
    // free the recipient table;
    [recipientsTable foreachExec:freeRecipient userData:NULL];
    
    [flags free];

    if (subject) [subject free];
    if (sentDate) [sentDate free];
    if (receivedDate)  [receivedDate free];
    if (from) [from free];
    if (flags) [flags free];
    if (recipientsTable) [recipientsTable free];
    
    return [super free];
}
@end
