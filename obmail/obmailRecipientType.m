/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailRecipientType.m : Recipient type (To, Cc, ....)  */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <obmailRecipientType.h>
#warning ##########
#warning FIXME: each predefined recipient type must return 
#warning already allocated objects. Not one for each demand.
#warning ##########

@implementation Obmail_Recipient_Type

+ to
{
  Obmail_Recipient_Type *instance;

  instance = [[Obmail_Recipient_Type alloc] init];
  instance->type = g_strdup("To");
  return instance;
}


+ cc
{
  Obmail_Recipient_Type *instance;

  instance = [[Obmail_Recipient_Type alloc] init];
  instance->type = g_strdup("Cc");
  return instance;

}


+ bcc
{
  Obmail_Recipient_Type *instance;

  instance = [[Obmail_Recipient_Type alloc] init];
  instance->type = g_strdup("Bcc");
  return instance;

}

/** getTypeString
 *  return the string key of the recipient type
 *
 * @Return value: a copy of the string key of the recipient type
 */
- (gchar *)getTypeString
{
    return g_strdup(self->type);
}

- free
{
    if (type) g_free(type);
    return self;
}

@end /** Obmail_Recipient_Type **/


