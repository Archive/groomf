/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailStore.h : Abstract class for a store                               */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <obmailStore.h>



@implementation Obmail_Store 

/**
 * getRootFolder : return the toplevel folder
 * 
 * Returns the folder which is at the top of the folder
 * hierarchy. This folder is generally different from
 * the default folder. 
 * 
 * @Return value: the tolevel folder.
 **/
- (Obmail_Folder *)getRootFolder
{
    return NULL;
}

/** 
 * getDefaultFolder : return the store default folder
 *
 * The default folder is the folder which is presented 
 * to the user in the default configuration. The default
 * is often the root folder.
 *
 *  @Return value: the default folder.
 **/
- (Obmail_Folder *)getDefaultFolder
{
    return NULL;
}


/** 
 * getFolder: return the folder corresponding to a path.
 * 
 * Returns the folder corresponding to the path "name". 
 * If the path begins with the separator caracter, it 
 * is relative to the root folder. Otherwise, it is
 * relative to the default folder.
 * The folder does not necessarily exist on the store.
 * To make sure it already exists, use its "exists" method.
 * If it does not exist, you can create it with its 
 * "create" method.
 *
 * @Return value: the folder
 **/
- (Obmail_Folder *)getFolder: (Obuli_String *)folderName
{
    Obmail_Folder *topFolder;
#warning fill this part in.
    return NULL;
}



/** 
 * setSeparator: set the character which separates this folder 
 * path from the folders names in a lower level of hierarchy.
 *
 * 
 *
 **/
- setSeparator: (gchar) sep
{
    separator = sep;
    return self;
}


/** 
 * getSeparator: return the character which separates this folder 
 * path from the folders names in a lower level of hierarchy.
 *
 * 
 *
 **/
- (gchar) getSeparator
{
    return separator;
}

@end


