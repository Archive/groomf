/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailAddress.h : Abstract class for an email address */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef OBMAIL_ADDRESS_H
#define OBMAIL_ADDRESS_H 1

#include <obmail.h>
#include <obuliCloneable.h>
#include <obuliString.h>

@interface Obmail_Address : Obuli_Thread_Object
< Obuli_Cloneable >
{
@protected
  /* stringRepresentation:
   * string representation of the address object.
   * specific subclasses *must* maintain this field
   * in order for the "toString" method to work
   */
  Obuli_String *stringRepresentation;

}
- (gboolean)equals: (Obmail_Address *)address;
- (Obuli_String *)getType;
- (Obuli_String *)toString;
- clone;

@end 

#endif /* OBMAIL_ADDRESS_H */

