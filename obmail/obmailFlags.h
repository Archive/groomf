/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailFlags.h : class implementing a message flag object */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef OBMAIL_FLAGS_H 
#define OBMAIL_FLAGS_H 1

#include <obmail.h>
#include <obuliCloneable.h>
#include <obuliThreadObject.h>
#include <obuliFlagHashTable.h>

@interface Obmail_Flags : Obuli_Thread_Object
< Obuli_Cloneable >
{
    Obuli_Flag_Hash_Table *hashTable;
}

- init;
- addFlag: (gchar *)name;
- addFlag: (gchar *)name
    value: (gboolean)value;
- setFlag: (gchar *)name
    value: (gboolean)value;
- (gboolean)containsFlag: (gchar *)name;
- (gboolean)getFlag: (gchar *)name;
- clone;

@end

#endif /** OBMAIL_FLAGS_H **/
