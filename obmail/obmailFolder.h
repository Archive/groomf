/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailFolder.h : Abstract class for an email folder */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef OBMAIL_FOLDER_H 
#define OBMAIL_FOLDER_H 1

#include <obmail.h>
#include <obmailMessage.h>
#include <obmailStore.h>

enum {
    FOLDER_OPEN,
    FOLDER_CLOSE
};

enum {
    FOLDER_OPEN_UNKNOWN,   /* folder open mode is unknown */
    FOLDER_OPEN_READ,      /* folder is read only         */ 
    FOLDER_OPEN_READ_WRITE /* folder is read/write        */ 
};

@class Obmail_Store;

@interface Obmail_Folder : Obuli_Thread_Object
{
    GList *messageList;
    gboolean canHoldFolders;
    gboolean canHoldMessages;
    gboolean existsOnStore;
    guint mode;
    guint openState;
    Obuli_String *name;
    Obmail_Store *parentStore;
    Obmail_Folder *parentFolder;
}

- initWithParentStore: (Obmail_Store *)pstore;
- appendMessage: (Obmail_Message *)msg;
- open;
- close: (gboolean)expunge;
- create;
- copyMessage: (Obmail_Message *)msg
	   to: (Obmail_Folder *)folder;
- setName: (Obuli_String *)name_string;
- (Obuli_String *)getName;
- (gboolean)canHoldFolders;
- (gboolean)canHoldMessages;
- (gboolean)isOpen;
- (Obmail_Folder *)getFolder: (Obuli_String *)folderName;

@end 


#endif /* OBMAIL_FOLDER_H */
