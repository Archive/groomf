/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailService.h : Abstract class for a service, ie the common      */
/* part between a store and a transport                               */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <obmailService.h>



@implementation Obmail_Service

/**
 * connect : connect to a service 
 *
 * connect to the service using the parameters 
 * stored in the session it is initialized with
 * WARNING: session not implemented for the moment
 **/
- connect
{
  
  return self;
}

/**
 * connectTo:login:password : connect to the specified address
 * 
 * Connect to the service, but do not use the session
 * default parameters to retrieve server's address
 *
 * @host: host to connect to
 * @user: user name used to log in
 * @pass: password used to log in
 **/
- connectTo: (Obuli_String *)host
      login: (Obuli_String *)user
   password: (Obuli_String *)pass
{
  [self setConnected:TRUE];
  return self;
}

- connectTo: (Obuli_String *)host
      login: (Obuli_String *)user
   password: (Obuli_String *)pass
       port: (guint)port
{
  [self setConnected:TRUE];
  return self;
}

- (gboolean)isConnected
{
  return connected;
}


- setConnected: (gboolean)state
{
  connected = state;
  return self;
}


@end

