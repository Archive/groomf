/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailAddress.m : Abstract class for an email address */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "obmailAddress.h"



@implementation Obmail_Address 


/** equals
 * tests if the adress equals to another one.
 * Subclasses may want  overload this method. This one
 * is only a case sensitive test using the toString method.
 * In particular, subclasses may test if the two adress objects
 * are the same type.
 *
 * Return value: TRUE if the two adresses equal, FALSE otherwise
 */
- (gboolean)equals: (Obmail_Address *)address
{
    Obuli_String *otherStr;
    
    otherStr = [address toString];  
    return [self->stringRepresentation equals:otherStr];
}



/** getType
 * returns a string that represents the address type
 *
 * Return value: the adress type 
 */
- (Obuli_String *)getType
{
    return [[Obuli_String alloc] initWith:"Abstract Adress Type"];
}


/** toString 
 * returns the string representation of the address object
 *
 * Return value: the string representation.
 */
- (Obuli_String *)toString
{
    return stringRepresentation;
}

/** initAsClone:
 * Internal use only. When an object is copied, 
 * instance variables are copied  only as a structure.
 * This method is here to copy the references of 
 * the instance variables pointer.
 **/
- initAsClone
{
#warning initAsCopy Method not tested
    stringRepresentation = [stringRepresentation clone];
    return self;
}
/** clone
 * deepcopy of the object. Each instance variable content is copied.
 * For exmaple, when strings are copied, the pointer is  replace by
 * a pointer onto a new copy of the string.
 *
`*/
- clone
{
#warning Object->copy method not tested yet 
    Obmail_Address *instanceClone;
    [self lock];
    instanceClone = [super copy];
    [instanceClone initAsClone];
    [self unlock];
    return instanceClone;
}

- free
{
    [stringRepresentation free];
    return [super free];
}
@end /** Obmail_Address **/














