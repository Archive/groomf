/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailMessage.h : Abstract class for an email message */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef OBMAIL_MESSAGE_H 
#define OBMAIL_MESSAGE_H 1

#include <obmail.h>
#include <obmailAddress.h>
#include <obmailFlags.h>
#include <obmailRecipientType.h>
#include <obuliString.h>
#include <obuliDate.h>
#include <obuliStrObjHashTable.h>

@interface Obmail_Message : Obuli_Thread_Object
< Obuli_Cloneable >
{

@protected
    Obuli_String *subject;
    Obuli_Date* sentDate;
    Obuli_Date* receivedDate;
    Obmail_Address *from;
    Obmail_Flags *flags;
    gint msgNumber;
    Obuli_Str_Obj_Hash_Table *recipientsTable;

    
}

- init;
- setFlag: (gchar *)name
    value: (gboolean)value;
- (gboolean)getFlag: (gchar *)name;
- setFrom: (Obmail_Address *)from;
- (Obmail_Address *)getFrom;
- setMessageNumber: (gint)msgnum;
- (gint)getMessageNumber;
- setRecipient: (gchar *)recType
       address: (Obmail_Address *)address;
- (Obmail_Address *)getRecipient: (gchar *)recType;
- setReceivedDate: (Obuli_Date *)date;
- (Obuli_Date *)getReceivedDate;
- setSentDate: (Obuli_Date *)date;
- (Obuli_Date *)getSentDate;
- clone;
@end


#endif /* OBMAIL_MESSAGE_H */
