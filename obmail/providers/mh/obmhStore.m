/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* obmh.m : mh store class*/

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <obmhStore.h>
#include <stdlib.h>

@implementation  Obmh_Store : Obmail_Store
{
	Obuli_String *toplevelDir; // toplevel directory of the mh store
	
}

/** 
 * init : init an mh store object
 *
 * Init a mh store object. Just like initWithToplevelDir
 * but the toplevel mh directory is determined
 * using the environnement profile component Path 
 * in $HOME/.mh_profile
 * If it is not defined, then, the second default
 * value is $HOME/Mail
 *
 **/



- initWithDefaults
{
	Obuli_String *obuliToplevel;
	gchar *homeDir;
	
# warning I have to implement a simple .mh_profile component parser	
	separator = '/';
	homeDir = (gchar *)getenv("HOME");
	if (homeDir) { // find the toplevel directory
		// find and parse .mh_profile
		 
		obuliToplevel = [[Obuli_String alloc] initWith:homeDir];
		[obuliToplevel appendChar:separator];
		[obuliToplevel appendChars:"Mail"];
		return [self initWithToplevelDir:obuliToplevel];			
	} else { // could not find any default MH toplevel directory
		g_warning("could not find a suitable default MH directory \n"
			  "neither the MAIL or HOME environement variable \n"
			  "are set.\n"); 
		return self;
	}	
	
	
}


- initWithToplevelDir: (Obuli_String *)tplvl
{
	[super init];
	toplevelDir = tplvl;
	separator = '/';
	return self;
}

- (Obmh_Folder *)getDefaultFolder
{
	//return toplevelDir;
	return NULL;
}

- (Obuli_String *)getToplevelDir
{
	return toplevelDir;
}

- (Obmh_Folder *)getFolder: (Obuli_String *)folderName
{
	Obmh_Folder *requiredFolder;
	g_assert(folderName);
	
	requiredFolder = [[Obmh_Folder alloc] initWithParentStore:self];
	
	return [requiredFolder setName:folderName];
}

@end








	 
