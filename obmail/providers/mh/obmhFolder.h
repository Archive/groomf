/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* obmh.h : mh folder class*/

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef OBMH_FOLDER_H 
#define OBMH_FOLDER_H 1

#include <obmh.h>
#include <obmailFolder.h>
#include <obmhStore.h>

@class Obmh_Store;

@interface Obmh_Folder : Obmail_Folder
{
	Obuli_String *directoryPath;
}

- initWithParentStore: (Obmail_Store *)pstore;
- initWithParentStore: (Obmail_Store *)pstore
		 name:  (Obuli_String *)name_string;
- setName: (Obuli_String *)name_string;
- (Obmh_Folder *)getFolder: (Obuli_String *)folderName;

@end


#endif /* OBMH_FOLDER_H */




