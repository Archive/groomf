/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* obmh.m : mh folder class*/

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */



#include <obmhFolder.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>


@implementation Obmh_Folder

- initWithParentStore: (Obmail_Store *)pstore
{
	[super initWithParentStore: pstore];
	return self;
}


/** 
 * initWithParentStore : convenience function 
 *
 **/
- initWithParentStore: (Obmail_Store *)pstore
		 name:  (Obuli_String *)name_string
{
	[self initWithParentStore: pstore];
	[self setName: name_string];
	return self;
}

- setName: (Obuli_String *)name_string
{
	Obuli_String *rootDirPath;
	Obuli_String *fullDirPath;
	gchar separator;

	g_assert(parentStore);
	[super setName:name_string];
	
	if (directoryPath) [directoryPath free];
	
	separator = [parentStore getSeparator];
	rootDirPath = [(Obmh_Store *)parentStore getToplevelDir];
	fullDirPath = [[[rootDirPath clone] appendChar:separator] append:name];

	directoryPath = fullDirPath;
	return self;

}

- create 
{
	gchar *fullDirName;
	mode_t dirMode=S_IRWXU;
	int creationError;
	
	g_assert(parentStore);
	g_assert(name);
	g_assert(directoryPath);


	if ([self exists]) return self;

	[super create];
	
	g_assert( fullDirName = [directoryPath charRepresentation] );
	
	creationError = mkdir(fullDirName, dirMode);
	if (creationError<0) {
		g_warning("Obmh_Folder::create, can not create folder %s", fullDirName);
	}
	
	return self;
}


- (Obmh_Folder *)getFolder: (Obuli_String *)folderName
{
	gchar separator;
	Obuli_String *tmpName;
	
	g_assert(parentStore);

	separator = [parentStore getSeparator];
	// construct the full name
	tmpName = [name clone];
	[tmpName appendChar:separator];
	[tmpName append:folderName];
	
	[folderName free];
	return (Obmh_Folder *)[parentStore getFolder:tmpName];
}


/** 
 * exists : tests if the folder object exists on the store.
 *
 **/
- (gboolean)exists
{

	DIR *dirStream;
	gboolean  ok;
	g_assert(directoryPath);

	dirStream = opendir( [directoryPath charRepresentation] );
	ok = (dirStream != NULL);
	if (ok) closedir(dirStream);
				
	return ok;
	
}


@end





