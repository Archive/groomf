/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailStore.h : Abstract class for a store                               */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef OBMAIL_STORE_H 
#define OBMAIL_STORE_H 1

#include <obmail.h>
#include <obuliString.h>
#include "obmailFolder.h"
#include "obmailService.h"

@class Obmail_Folder;

@interface Obmail_Store : Obmail_Service
{
    gchar separator;

}
- (Obmail_Folder *)getDefaultFolder;
- (Obmail_Folder *)getFolder: (Obuli_String *)folderName;
- setSeparator: (gchar) sep;
- (gchar) getSeparator;

@end

#endif /* OBMAIL_STORE_H */
