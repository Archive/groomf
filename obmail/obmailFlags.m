/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailFlags.m : class implementing a message flag object */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <obmailFlags.h>

@implementation Obmail_Flags

/** init:
 * Constuctor. Initializes the flag object and allocate
 * the underlying  GHashTable structure 
 *
 */ 

- init
{
  [super init];
  hashTable = [[Obuli_Flag_Hash_Table alloc] init];
  return self;
}

/** addFlag:
 * Adds a new flag. Its value is set to FALSE.
 * This method calls the addFlag:: method with a FALSE value. 
 *
 * @name: name of the flag. 
 */
- addFlag: (gchar *)name
{
  [self addFlag:name value:FALSE];
  return self;
}

/** addFlag:
 * Adds a new flag and sets its value.
 * If it already exists, it is destroyed and both
 * the corresponding name and value structures are freed. 
 *
 * @name: name of the flag.
 * @value: initial value of the flag
 */
- addFlag: (gchar *)name
    value: (gboolean)value
{
  gpointer origName;
  gpointer origValue;
  gboolean origExists;
  gboolean *dynValue;

  g_assert(name);
  [hashTable lock];
  origExists = [hashTable lookupExtendedKey:(id)name origKey:(id *)&origName origValue:(id *)&origValue];
  // In the case where the user adds an already existing 
  // flag, we have to free the old one.
  if (origExists)
    {
      g_free(origValue);
      g_free(origName);
    }
  
  dynValue = g_new(gboolean,1);
  *dynValue = value;
  [hashTable insertKey:(gpointer)g_strdup(name) value:(gpointer)dynValue ];
  [hashTable unlock];
  return self;
}

/** setFlag
 * sets the value of a flag.
 *
 * @name: name of the flag
 * @value: value of the flag
 */
- setFlag: (gchar *)name
    value: (gboolean)value
{
  gboolean  *dynValue;
  [hashTable lock];
  dynValue = (gboolean *)[hashTable lookupKey:(gpointer)name];

  if (!dynValue) return self;
  *dynValue = value;
  [hashTable unlock];
  return self;
}

/** containsFlag
 * tests if the object contains a flag.
 *
 * @name: name of the flag to search for.
 */
- (gboolean)containsFlag: (gchar *)name
{
  g_assert(name);
  return ([hashTable lookupKey:(gpointer)name] != NULL);
}

/** getFlag:
 * returns the value of a flag. If the flag does not exist
 * in the current Flags object, the result is FALSE.
 *
 * @Return Value: value of the flag.
 */
- (gboolean)getFlag: (gchar *)name
{
  gboolean *dynValue;
  gboolean returnValue;
  g_assert(name);
  [hashTable lock];
  dynValue = (gboolean *)[hashTable lookupKey:(gpointer)name];
  
  if (dynValue)
    returnValue = *dynValue;
  else
    returnValue= FALSE;
  [hashTable unlock];
  return returnValue;
}



- initAsClone
{
   
    hashTable =  [hashTable clone];
    return self;
}


- clone
{
    Obmail_Flags *instanceClone;
    [hashTable lock]; 
    instanceClone = [super copy];
    [instanceClone initAsClone];
    [hashTable unlock]; 
    return instanceClone;
}


void  freeFlag(gpointer	key, gpointer	value, gpointer	user_data)
{
    g_free(key);
    g_free(value);
}

-free
{
    [hashTable foreachExec:freeFlag userData:NULL];
    [hashTable free];
    return [super free];
}

@end /** Obmail_Flags **/

