/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obmailFolder.m : Abstract class for an email folder */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <obmailFolder.h>

@implementation Obmail_Folder

/**
 * initWithParentStore : initialize the folder 
 * and set its parent store
 * 
 * Initalize the folder and set the store object 
 * in which it is stored. the store can be NULL;
 * The folder is return in a closed state
 *
 * @pstore: the parent store
 *
 **/ 
- initWithParentStore: (Obmail_Store *)pstore
{
    [super init];
    parentStore = pstore;
    return self;
}
/** 
 * init : init a folder object.
 *
 * Init a folder object without assigning it
 * a parent store.
 *
 */
- init
{    
    [self initWithParentStore: NULL];
    return self;
}


/**
 * appendMessage: appends a message to the folder
 *
 * Append a message to the folder message list. It 
 * is stored as the last message of the list. 
 * This is only a "caching storage" of the message list.
 * subclasses must overload this method in order to 
 * really append specific folders. 
 *
 * @msg: the message to append to the folder
 **/
- appendMessage: (Obmail_Message *)msg
{
    Obmail_Message *newCopy;
    newCopy = [msg clone];
    messageList = g_list_append( messageList, (gpointer)newCopy);
    return self;
}

/** 
 * open: put a folder in its opened state. 
 *
 * Open a folder.
 * 
 **/ 
- open
{
    openState = FOLDER_OPEN;
    return self;
}


/** 
 * close: put a folder in its closed state. 
 *
 * Close a folder, and possibly expunge the 
 * flagged messages.
 * 
 * @expunge: if TRUE, the flagged message are
 * deleted.
 **/ 
- close: (gboolean)expunge
{
#warning implement the expunge flag
    openState = FOLDER_CLOSE;
    return self;
}

/** 
 * copyMessage: copy a message to a folder
 * 
 * copy a message from this folder to another
 * folder. The message is duplicated.
 * 
 * @msg: the message to copy
 * @folder: the destination folder
 **/
- copyMessage: (Obmail_Message *)msg
	   to: (Obmail_Folder *)folder
{
    [folder appendMessage:msg];
    return self;
}
/** 
 * setName : set the  name of the folder
 *
 * set the name of the folder. 
 * The old name object is freed.
 *
 * @name_string: new name of the folder
 *
 **/
- setName: (Obuli_String *)name_string
{
    if (name) [name free];
    name = name_string;
    return self;
}


/** 
 * getName : get the (short) name of the folder
 *
 * get the name of the folder. The fully qualified name
 * must can be obtained with the getFullName method (not implemented)
 *
 * @Return Value: name of the folder
 *
 **/
- (Obuli_String *)getName
{
    return name;
}

/**
 * canHoldFolders : tests if the folder can contain other folders
 *
 **/
- (gboolean)canHoldFolders
{
    return canHoldFolders;
}

/** 
 * canHoldMessages : tests if the folder can contain messages
 *
 **/
- (gboolean)canHoldMessages
{
    return canHoldMessages;
}


/** 
 * exists : tests if the folder object exists on the store.
 *
 **/
- (gboolean)exists
{
    
    return existsOnStore;
}


/** 
 * isOpen : test if the folder is open
 *
 **/
- (gboolean)isOpen
{
    return (openState==FOLDER_OPEN);
}



/** 
 * getFolder: return the (sub)folder object that
 * is specified.
 *
 * This method returns a folder objects. This folder
 * is necessarily a subfolder of the current folder. 
 * It is an error to ask a folder begining with the 
 * folder separator character.  
 * 
 * @folderName: subfolder path. NULL if the subfolder object
 *        could not be created
 **/
- (Obmail_Folder *)getFolder: (Obuli_String *)folderName
{
    printf("getFolder called on the abstract folder class\n");
    return NULL;
}

/** 
 * create : create the folder object on the physical stire
 *
 * This routine physically creates the folder object on 
 * the store. Having created the (objc) object does not
 * mean the folder physically exists. If it does not
 * exists, this routine will create it.
 * if the folder pathname contains more than one level
 * of hierarchy, all folders between the current folder
 * and the last folder name will be created if not existing.
 *
 **/
- create
{
    Obuli_String *prefix;
    gchar dich_result;
    Obmail_Folder *parent;
    
    g_assert(parentStore);
    g_assert(name);
    
    if ([self exists]) return self;
    if (parentFolder)  [parentFolder create];
    else {   
	
	dich_result = [name rightDichotomy:[parentStore getSeparator] 
			    prefix:&prefix 
			    suffix:NULL 
			    options:NONE]; 
	if (dich_result!='o') {
	    g_warning("I have to handle the case where the path is not OK\n"); 
	    return self;
	} else {
	    parent = [parentStore getFolder:prefix];
	    [parent create];
	    [parent free];
	}
    }
    
    return self;	
	
    

}

@end


