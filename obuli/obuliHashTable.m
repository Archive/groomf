/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obuliHashTable.m : Hash table class */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */



#include <obuliHashTable.h>

@implementation Obuli_Hash_Table

- initWithHashFunc: (GHashFunc)hf
       CompareFunc: (GCompareFunc)cf
{
  __hashTable = g_hash_table_new(hf, cf);
  hashFunc = hf;
  compareFunc = cf;
  return self;
}

- insertKey: (id)key
      value: (id)value
{
  g_hash_table_insert( __hashTable, (gpointer)key, (gpointer)value);
  return self;
}

- removeKey: (id)key
{
  g_hash_table_remove( __hashTable, key);
  return self;
}


- (id)lookupKey: (id)key
{
  return (id)g_hash_table_lookup( __hashTable, key);
}


- (gboolean)lookupExtendedKey: (id)key
		      origKey:(id *)origKey
		      origValue: (id *)origValue
{
  return g_hash_table_lookup_extended( __hashTable, key, 
				       (gpointer *)origKey, 
				       (gpointer *)origValue);
}


- foreachExec: (GHFunc)func
     userData: (id)userData
{
  g_hash_table_foreach(__hashTable, func, userData);
  return self;
}


- copyPair:(gpointer) key
  :(gpointer)value
  :(gpointer)userData
{
  GHashTable *newHashTable;
  id newKey;
  id newValue;
  
  newValue = [(id)value clone];
  newKey = [(id)key clone];
  newHashTable = (GHashTable *)userData;
  g_hash_table_insert( newHashTable, (gpointer)newKey, (gpointer)newValue);
  return self;
}

typedef struct
{
  id object;
  gpointer user_data;
  
} C_user_data;

void C_copyPair(gpointer key, gpointer value, gpointer CUserData)
{
  [ ((C_user_data *)CUserData)->object 
		   copyPair:key 
		   :value 
		   :((C_user_data *)CUserData)->user_data ];
} 


- initAsClone
{
  C_user_data CUserData;
  GHashTable *newHashTable;

  newHashTable = g_hash_table_new(hashFunc, compareFunc);
  CUserData.object = self;
  CUserData.user_data = (gpointer)newHashTable;
  g_hash_table_foreach(__hashTable, C_copyPair, &CUserData);
  __hashTable = newHashTable;
  return self;
}

- clone
{
    Obuli_Hash_Table *instanceClone;

    instanceClone = [super copy];
    [instanceClone initAsClone];
    return instanceClone;
}

- free
{
  return [super free];
}

@end

