/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obuliDate.m : Date class */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <obuliDate.h>

@implementation Obuli_Date

- init
{
    [super init];
    __date = g_date_new();
    return self;
}


- initWithGDate: (GDate *)gdate
{
    [super init];
    __date = gdate;
    return self;
    
}


- initWithDay: (GDateDay)day
	month:(GDateMonth)month
	 year:(GDateYear)year
{
    [super init];
    __date = g_date_new_dmy(day, month, year);
    return self;

}


- (gboolean)valid
{
    return g_date_valid(__date);
}

/** initAsClone:
 * Internal use only. When an object is copied, 
 * instance variables are copied  only as a structure.
 * This method is here to copy the references of 
 * the instance variables pointer.
 **/
- initAsClone
{
    GDate *newDate;
    newDate = g_new(GDate,1);
    memcpy(newDate, __date, sizeof(GDate));
    __date = newDate;
    return self;
}

- clone
{
    Obuli_Date *instanceClone;

    instanceClone = [super copy];
    [instanceClone initAsClone];
    return instanceClone;
}


- free
{
    g_date_free(__date);
    return [super free];
}

@end

