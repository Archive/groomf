/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* obuliString.h : string class */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef OBULI_STRING_H
#define OBULI_STRING_H 1

#include <obuli.h>
#include <obuliCloneable.h>
#include <obuliThreadObject.h>

typedef enum {
    NONE            =     0,
    STRIP_TRAILING  =     1,
    STRIP_LEADING   =     2
    
} DichotomyOption;


@interface Obuli_String : Obuli_Thread_Object
<Obuli_Cloneable>
{
  GString *__string;
}

- init;
- initWith: (gchar *)str;
- (gchar *)charRepresentation;
- (gboolean)equals: (Obuli_String *)otherString;
- append: (Obuli_String *)otherString;
- appendChars: (gchar *)chars;
- appendChar: (gchar)c;
- (gchar **)split:(gchar *) delimiter;
- (gchar)rightDichotomy: (gchar )sep
		 prefix: (Obuli_String **)prefix
		 suffix: (Obuli_String **)suffix
		options: (DichotomyOption)options;
- clone;
- free;
@end


#endif /* OBULI_STRING_H */
