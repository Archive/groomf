/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obuliDate.h : Date class */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef OBULI_DATE_H
#define OBULI_DATE_H 1

#include <obuli.h>
#include <obuliCloneable.h>
#include <obuliThreadObject.h>

@interface Obuli_Date : Obuli_Thread_Object
<Obuli_Cloneable>
{
  GDate *__date;
}

- init;
- initWithGDate: (GDate *)gdate;
- initWithDay: (GDateDay)day
	month:(GDateMonth)month
	 year:(GDateYear)year;
- (gboolean)valid;
- clone;
- free;

@end


#endif /* OBULI_DATE_H */
