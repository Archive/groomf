/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef OBULI_LOG_H
#define OBULI_LOG_H


#include <stdarg.h>
#include <glib.h>
#include <stdio.h>

extern int global_debug_level;
extern FILE *log_file_descriptor;

typedef enum {
	NO_LOG     =     0,
	FULL_DEBUG =     10
} LogLevel;
	
#define OBULI_LOG(level, args...) obuli_log(level,##args);

extern void obuli_log(LogLevel level, const gchar *format, ... );

#endif /* OBULI_LOG_H */
