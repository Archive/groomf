/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obuliHashTable.h : Hash table class */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef OBULI_HASH_TABLE_H
#define OBULI_HASH_TABLE_H 1

#include <obuli.h>
#include <obuliCloneable.h>
#include <obuliThreadObject.h>

@interface Obuli_Hash_Table : Obuli_Thread_Object
<Obuli_Cloneable>
{
    GHashTable *__hashTable;
    GHashFunc hashFunc;
    GCompareFunc compareFunc;
}

- initWithHashFunc: (GHashFunc)hf
       CompareFunc: (GCompareFunc)cf;
- insertKey: (id)key
      value: (id)value;
- removeKey: (id)key;
- (id)lookupKey: (id)key;
- (gboolean)lookupExtendedKey: (id)key
		      origKey:(id *)origKey
		    origValue: (id *)origValue;
- foreachExec: (GHFunc)func
     userData: (id)userData;
- clone;
- free;

@end


#endif /* OBULI_HASH_TABLE_H */
