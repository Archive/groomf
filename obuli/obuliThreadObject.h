/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* obuliThreadObject.h : Abstract class for a generic object mutex-lockable */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef OBULI_THREAD_OBJECT_H
#define OBULI_THREAD_OBJECT_H 1

#include <obuli.h>

#define obuli_mutex objc_mutex_t
#define obuli_mutex_new objc_mutex_allocate
#define obuli_mutex_free objc_mutex_deallocate
#define obuli_mutex_lock objc_mutex_lock
#define obuli_mutex_trylock objc_mutex_trylock
#define obuli_mutex_unlock objc_mutex_unlock

#define obuli_thread_detach objc_thread_detach
#define obuli_thread_exit objc_thread_exit

@interface Obuli_Thread_Object : Object
{
@protected
    obuli_mutex objectLock;
}

- lock;
- unlock;
- (gboolean)tryLock;
@end 

#endif /* OBULI_THREAD_OBJECT_H */

