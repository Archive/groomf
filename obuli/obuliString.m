/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* obuliString.m : string class */

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */



#include <obuliString.h>

@implementation Obuli_String 
- init;
{
  return [self initWith:"\0"];
}


- initWith: (gchar *)str
{
    [super init];
    __string = g_string_new(str);
    return self;
}

- (gchar *)charRepresentation
{
  return __string->str;
}

- (gboolean)equals: (Obuli_String *)otherString
{
    return !strcmp([self charRepresentation], [otherString charRepresentation]);
}


- (gchar **)split:(gchar *) delimiter
{
    return g_strsplit( [self charRepresentation], delimiter, -1);
}

/**
 * rightDichotomy : return the strings before and/or after 
 * the last occurence of the specified separator
 *
 * This routine returns the string before and/or after
 * a character given as an argument. 
 * if the separator is the last character, prefix and/or
 * suffix is set to NULL and result is set to 'l'
 * if the separator is not in the list, prefix and/or
 * suffix is set to NULL and result is set to 'n'
 * When the operation succedeed, the return value is 'o'
 *
 * @sep : separator
 * @prefix: pointer to be field by the prefix object
 *   the prefix is not returned when the given pointer is NULL
 * @suffix: pointer to be field by the suffix object
 *   the suffix is not returned when the given pointer is NULL
 *
 * @Return Value : result of the operation ('o', 'l' or 'n')
 *
 **/
- (gchar)rightDichotomy: (gchar )sep
		 prefix: (Obuli_String **)prefix
		 suffix: (Obuli_String **)suffix
		options: (DichotomyOption)options
{
    gchar *str, *tmp;
    gint pos, len, first;
    
    OBULI_LOG(FULL_DEBUG,\
	      "Entering rightDichotomy: \n\tseparator=%c \n\tprefix=%p \n\tsuffix=%p \n\toptions=%ld\n",\
	      sep, prefix, suffix, options)
    g_assert( tmp=[self charRepresentation]  );
    len = strlen(tmp);
    if (!len) {
	    if (prefix) *prefix=NULL;
	    if (suffix) *suffix=NULL;
	    OBULI_LOG(FULL_DEBUG,"rightDichotomy: string is empty\n")
	    return 'n';
    }
    first = 0;

    if ( (options & STRIP_LEADING ) && (tmp[first] == sep) )
	    do {first++;} while ( (first<len) && (tmp[first] == sep) );
    
    if (options & STRIP_TRAILING )
    	    while (tmp[len-1] == sep)
		    len--;
    
    if (first==len) {
	    if (prefix) *prefix=NULL;
	    if (suffix) *suffix=NULL;
	    OBULI_LOG(FULL_DEBUG,"rightDichotomy: after stripping, string is empty\n")
	    return 'n';
    }

    pos = len;
    
    do {
	pos--;
    } while ((pos>=first) && (tmp[pos]!=sep));


    if (pos<first) 
	{
	    if (suffix) *suffix=NULL;
	    if (prefix) *prefix=NULL;
	    OBULI_LOG(FULL_DEBUG,"rightDichotomy: separator not found\n")
	    return 'n';
	}
    
    // if we have stripped trailongs separators, we should
    // never enter here
    if (pos==len-1) 
	{
	    if (suffix) *suffix=NULL;
	    if (prefix) *prefix=NULL;
	    OBULI_LOG(FULL_DEBUG,"rightDichotomy: separator is last character\n")
	    return 'l';
	}
    
    if (prefix) // return the prefix
	{
	    str = g_strndup(tmp,pos);
	    *prefix = [[Obuli_String alloc] initWith:(str)];
	    g_free(str);
	}
    if (suffix) // return the suffix
	{
	    str = g_strdup(tmp+pos+1);
	    *suffix = [[Obuli_String alloc] initWith:(str)];
	    g_free(str);
	}
    
    return 'o';
}


/** 
 * append : append a string object
 *
 * Append a string object. The appended string
 * is left untouched nor freed.
 *
 * @otherString: the string to append
 *
 **/
- append: (Obuli_String *)otherString
{
    g_assert(otherString);
    g_assert([otherString charRepresentation]);
    [self appendChars:([otherString charRepresentation])];
    return self;
}


/**
 * appendChars: append a character string
 *
 * Append a character string at the end of the 
 * string object. The character string is not touched 
 * nor freed.
 *
 * @chars: character string to append
 * 
 **/
- appendChars: (gchar *)chars
{
    __string = g_string_append(__string, chars);
    return self;
}

/**
 * appendChar: append a character
 *
 * Append a character at the end of the string object.
 *
 * @chars: character to append
 * 
 **/
- appendChar: (gchar)c
{
    __string = g_string_append_c(__string, c);
    return self;
}


/** 
 * initAsClone:
 * Internal use only. When an object is copied, 
 * instance variables are copied  only as a structure.
 * This method is here to copy the references of 
 * the instance variables pointer.
 **/
- initAsClone
{
    __string = g_string_new( g_strdup(__string->str) );
    return self;
}


/** clone:
 * deep clone method.
 * 
 * This method returns a clone of the object. All 
 * its instance variables are also cloned.
 *
 * @Return value: an object's clone.
 **/
- clone
{  
    Obuli_String *instanceClone;

    instanceClone = [super copy];
    [instanceClone initAsClone];
    return instanceClone;
}


- free
{
  g_string_free(__string, 0);
  return [super free];
}
@end
