/* -*- Mode: ObjC; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* test-1-mh.m : test for mh provider*/

/* 
 *
 * Copyright (C) 1999 Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr> .
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#define TOPLEVEL_DIR_TEST "/tmp/mh_test"
#define SIMPLE_TEST_NAME "folder"
#define SIMPLE_TEST_NAME2 "subfolder"


#include "obmhFolder.h"
#include "obmhStore.h"

int
main(int argc, char **argv)
{
  Obmh_Store *mhStore;
  Obmh_Folder *mhFolder, *mhSubFolder;
  Obuli_String *topLevelDir;
  Obuli_String *simpleName, *simpleName2;
  
  // create a simple mailbox dir name
  simpleName = [[Obuli_String alloc] initWith:SIMPLE_TEST_NAME];
  simpleName2 = [[Obuli_String alloc] initWith:SIMPLE_TEST_NAME2];

  // create a sample (non default) mh store
  g_print(" * create a sample (non default) mh store ...\n");
  topLevelDir = [[Obuli_String alloc] initWith:TOPLEVEL_DIR_TEST];
  mhStore = [[Obmh_Store alloc] initWithToplevelDir:topLevelDir];
  // create a sample folder in it
  mhFolder = [[Obmh_Folder alloc] initWithParentStore:mhStore];
  [mhFolder setName:[simpleName clone]];
  [mhFolder create];
  [mhFolder free];
  [mhStore free];
  g_print("  ...  Finished\n");

  // create a default mh store
  g_print(" * create a default mh store ...\n");
  mhStore = [[Obmh_Store alloc] initWithDefaults];
  // create a sample folder in it, testing the 
  // methid Obmh_Store::getFolder
  mhFolder = [mhStore getFolder:[simpleName clone]];
  [mhFolder create];
  g_print("  ... Finished\n");

  // create a subfolder in the new folder
  g_print(" * create a subfolder in the default mh store ...\n");
  mhSubFolder = [mhFolder getFolder:[simpleName2 clone]];
  [mhSubFolder create];
  [mhSubFolder free];
  [mhFolder free];
  [mhStore free];
  g_print("  ... Finished\n");


  [simpleName2 free];
  [simpleName free];
  

}
